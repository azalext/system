# -*- coding: utf-8 -*-

from azalinc.shortcuts import *

from azalinc.node.models import *

#*********************************************************************

BACKEND_TYPEs = (
    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

################################################################################

class InstructionSet(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)
    count    = models.PositiveIntegerField(default=32)

    run_also = models.ForeignKey("InstructionSet", related_name='compat', blank=True, null=True)

    def __str__(self): return str(self.alias)

#*******************************************************************************

class ChipsetFamily(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    has_also = models.ForeignKey("ChipsetFamily", related_name='compat', blank=True, null=True)

    def __str__(self): return str(self.alias)

################################################################################

class HardwareAccessory(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    usage   = models.ForeignKey("HardwareAccessory", related_name='compat', blank=True)

    def __str__(self): return str(self.alias)

#*******************************************************************************

class HardwareProfile(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    can_also = models.ForeignKey("HardwareProfile", related_name='compat', blank=True)
    has_also = models.ForeignKey("HardwareAccessory", related_name='require', blank=True)
    use_also = models.ForeignKey("HardwareAccessory", related_name='include', blank=True)

    def __str__(self): return str(self.alias)

################################################################################

class DesktopEnv(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

#*******************************************************************************

class OperatingSystem(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    architec = models.ManyToManyField(InstructionSet, related_name='distros', blank=True)
    based_on = models.ForeignKey("OperatingSystem", related_name='distros', blank=True, null=True, default=None)
    desktops = models.ManyToManyField(DesktopEnv, related_name='desktop', blank=True)

    def __str__(self): return str(self.alias)

#*******************************************************************************

class CodeRuntime(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    archi    = models.ManyToManyField(InstructionSet, related_name='runtimes', blank=True)
    opsys    = models.ManyToManyField(OperatingSystem, related_name='runtimes', blank=True)

    def __str__(self): return str(self.alias)

