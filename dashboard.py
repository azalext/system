from controlcenter import Dashboard, widgets

from azalext.system.models import *

################################################################################

class AccessoryList(widgets.ItemList):
    model = HardwareAccessory
    list_display = ('alias','title')

#*******************************************************************************

class ProfileList(widgets.ItemList):
    model = HardwareProfile
    list_display = ('alias','title')

#*******************************************************************************

class OperatingList(widgets.ItemList):
    model = OperatingSystem
    list_display = ('alias','title')

################################################################################

class InstructionList(widgets.ItemList):
    model = HardwareProfile
    list_display = ('alias','title','count')

#*******************************************************************************

class InstructionMapping(widgets.SingleBarChart):
    # label and series
    values_list = ('title','count')
    # Data source
    queryset = InstructionSet.objects.all()

################################################################################

class Landing(Dashboard):
    title = 'System'

    widgets = (
        InstructionList,
        #InstructionMapping,

        AccessoryList,
        ProfileList,

        OperatingList,
    )

