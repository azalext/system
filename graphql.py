import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

##########################################################################

class InstructionType(DjangoObjectType):
    class Meta:
        model = InstructionSet
        filter_fields = ['alias','title','count','run_also']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class ChipsetType(DjangoObjectType):
    class Meta:
        model = ChipsetFamily
        filter_fields = ['alias','title','has_also']
        interfaces = (graphene.relay.Node, )

##########################################################################

class AccessoryType(DjangoObjectType):
    class Meta:
        model = HardwareAccessory
        filter_fields = ['alias','title','usage']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class ProfileType(DjangoObjectType):
    class Meta:
        model = HardwareProfile
        filter_fields = ['alias','title','can_also','has_also','use_also']
        interfaces = (graphene.relay.Node, )

##########################################################################

class DesktopType(DjangoObjectType):
    class Meta:
        model = DesktopEnv
        filter_fields = ['alias','title']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class OperatingType(DjangoObjectType):
    class Meta:
        model = OperatingSystem
        filter_fields = ['alias','title','architec','based_on','desktops']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class RuntimeType(DjangoObjectType):
    class Meta:
        model = CodeRuntime
        filter_fields = ['alias','title','archi','opsys']
        interfaces = (graphene.relay.Node, )

##########################################################################

class MachineType(DjangoObjectType):
    class Meta:
        model = Machine
        filter_fields = ['user','name','kind','runs','hw_ddr','ip4addr','ip6addr','username','password']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class DeviceType(DjangoObjectType):
    class Meta:
        model = Device
        filter_fields = ['parent','narrow','driver','hw_ddr','ip4addr','ip6addr','username','password']
        interfaces = (graphene.relay.Node, )

##########################################################################

class HardwareQuery(graphene.ObjectType):
    one_process = graphene.relay.Node.Field(InstructionType)
    all_process = DjangoFilterConnectionField(InstructionType)

    one_chipset = graphene.relay.Node.Field(ChipsetType)
    all_chipset = DjangoFilterConnectionField(ChipsetType)

    one_accessory = graphene.relay.Node.Field(AccessoryType)
    all_accessory = DjangoFilterConnectionField(AccessoryType)

    one_profile = graphene.relay.Node.Field(ProfileType)
    all_profile = DjangoFilterConnectionField(ProfileType)

#*********************************************************************

class SoftwareQuery(graphene.ObjectType):
    one_desktop = graphene.relay.Node.Field(DesktopType)
    all_desktop = DjangoFilterConnectionField(DesktopType)

    one_opersys = graphene.relay.Node.Field(OperatingType)
    all_opersys = DjangoFilterConnectionField(OperatingType)

    one_runtime = graphene.relay.Node.Field(RuntimeType)
    all_runtime = DjangoFilterConnectionField(RuntimeType)

#*********************************************************************

class Query(SoftwareQuery,HardwareQuery):
    one_machine = graphene.relay.Node.Field(MachineType)
    all_machine = DjangoFilterConnectionField(MachineType)

    one_device = graphene.relay.Node.Field(DeviceType)
    all_device = DjangoFilterConnectionField(DeviceType)

#*********************************************************************

schema = graphene.Schema(query=Query)

