# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

################################################################################

class InstructionAdmin(admin.ModelAdmin):
    list_display  = ['alias','title','count']
    list_filter   = ['count']
    #list_editable = ['title']

admin.site.register(InstructionSet, InstructionAdmin)

#*******************************************************************************

class ChipsetAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(ChipsetFamily, ChipsetAdmin)

################################################################################

class AccessoryAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(HardwareAccessory, AccessoryAdmin)

#*******************************************************************************

class ProfileAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(HardwareProfile, ProfileAdmin)

################################################################################

class DesktopAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(DesktopEnv, DesktopAdmin)

#*******************************************************************************

class SystemAdmin(admin.ModelAdmin):
    list_display  = ['alias','title','based_on'] # ,'tools','count']
    #list_filter   = ['architec__alias','architec__count','desktops__alias','based_on__alias']
    list_filter   = ['architec__alias','desktops__alias','based_on__alias']
    #list_editable = ['name','fqdn']

    def tools(self, record):
        resp = {}

        resp['View'] = "/shops/%(id)s/"        % record.__dict__
        resp['Sync'] = "/shops/%(id)s/refresh" % record.__dict__

        return '&nbsp;'.join(['<a href="%s">%s</a>' % (v,k) for k,v in resp.iteritems()])
    tools.allow_tags = True

    def count(self, record):
        return len(record.datasets.all())

admin.site.register(OperatingSystem, SystemAdmin)

################################################################################


