from azalinc.shortcuts import *

from azalinc.node.models import *

#from azalext.legals.models import *
#from azalext.govern.models import *
#from azalext.health.models import *
from azalext.legals.models import *
#from azalext.prints.models import *
from azalext.retail.models import *
from azalext.system.models import *

class Command(DataSeed):
    """
    Runs RQ workers on specified queues. Note that all queues passed into a
    single rqworker command must share the same connection.

    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<queue queue ...>'

    def initialize(self, parser):
        parser.add_argument('--worker-class', action='store', dest='worker_class',
                            help='RQ Worker class to use')
        parser.add_argument('--pid', action='store', dest='pid',
                            default=None, help='PID file to write the worker`s pid into')
        parser.add_argument('--burst', action='store_true', dest='burst',
                            default=False, help='Run worker in burst mode')
        parser.add_argument('--name', action='store', dest='name',
                            default=None, help='Name of the worker')
        parser.add_argument('--queue-class', action='store', dest='queue_class',
                            help='Queues class to use')
        parser.add_argument('--job-class', action='store', dest='job_class',
                            help='Jobs class to use')
        parser.add_argument('--worker-ttl', action='store', type=int,
                            dest='worker_ttl', default=420,
                            help='Default worker timeout to be used')
        parser.add_argument('--sentry-dsn', action='store', default=None, dest='sentry-dsn',
                            help='Report exceptions to this Sentry DSN')

    #************************************************************************************

    def launching(self, *args, **options):
        self.PAYLOAD = [
(self.core_lang,Language,"Ensuring languages"),

(self.auth_users,Person,"Ensuring identities"),
(self.auth_organ,Organism,"Ensuring organisms"),
#(self.auth_agent,Agent,"Ensuring bot agents"),

(self.chr_archit,InstructionSet,"Populating Instruction Sets"),
(self.chr_chipset,ChipsetFamily,"Populating Chipset Families"),

#(self.chr_access,HardwareAccessory,"Populating Hardware accessories"),
#(self.chr_profile,HardwareProfile,"Populating Hardware profiles"),

(self.chr_desktop,DesktopEnv,"Populating Desktop environment"),
(self.chr_system,OperatingSystem,"Populating Operating Systems"),
        ]

    #####################################################################################

    def chr_archit(self,model,title, **options):
        self.chrome['arch'] = {}

        for cfg in tqdm([
            dict(alias='avr', title='AVR', count=8),
            dict(alias='arc', title='ARC', count=16),

            dict(alias='mips', title='MIPS', count=32),
            dict(alias='power', title='Power-PC', count=32),
            dict(alias='sparc', title='Sparc-PC', count=32),

            dict(alias='i686', title='i686', count=32),
            dict(alias='x86', title='x86', count=32),
            dict(alias='x64', title='x86_64', count=64),
        ], desc=title):
            self.chrome['arch'][cfg['alias']],rsp = model.objects.get_or_create(alias=cfg['alias'])

            self.chrome['arch'][cfg['alias']].title = cfg['title']
            self.chrome['arch'][cfg['alias']].count = cfg['count']

            self.chrome['arch'][cfg['alias']].save()

    #************************************************************************************

    def chr_chipset(self,model,title, **options):
        self.chrome['chip'] = {}

        for key,lbl in tqdm(dict(
            amd='AMD',
        ).iteritems(), desc=title):
            self.chrome['chip'][key],rsp = model.objects.get_or_create(alias=key)

            self.chrome['chip'][key].title = lbl

            self.chrome['chip'][key].save()

    #************************************************************************************

    def chr_desktop(self,model,title, **options):
        self.chrome['desk'] = {}

        for key,lbl in tqdm(dict(
            gnome='Gnome Desktop',unity='Unity',
            lxde='L.X.D.E', xfce='Xfce', zh='K.D.E',
        ).iteritems(), desc=title):
            self.chrome['desk'][key],rsp = model.objects.get_or_create(alias=key)

            self.chrome['desk'][key].title = lbl

            self.chrome['desk'][key].save()

    #************************************************************************************

    def chr_system(self,model,title, **options):
        self.chrome['osys'] = {}

        for cfg in tqdm([
            dict(alias='debian',title="Debian",desk=['gnome','unity'],arch=['i686','x86','x64']),
            dict(alias='ubuntu',title="Ubuntu",based='debian',desk=['gnome','unity'],arch=['i686','x86','x64']),

            dict(alias='redhat',title="Redhat Entreprise Linux",desk=['gnome','unity'],arch=['i686','x86','x64']),
            dict(alias='centos',title="Cent OS Server",based='redhat',desk=['gnome','unity'],arch=['i686','x86','x64']),
            dict(alias='fedora',title="Fedora Project",based='redhat',desk=['gnome','unity'],arch=['i686','x86','x64']),
        ], desc=title):
            key = cfg['alias']

            self.chrome['osys'][key],rsp = model.objects.get_or_create(alias=key)

            if 'based' in cfg:
                if cfg['based'] in self.chrome['osys']:
                    self.chrome['osys'][key].based_on = self.chrome['osys'][cfg['based']]

            self.chrome['osys'][key].title = cfg.get('title',key.capitalize())

            for nrw in cfg.get('arch',[]):
                if nrw in self.chrome['arch']:
                    self.chrome['osys'][key].architec.add(self.chrome['arch'][nrw])

            for nrw in cfg.get('desk',[]):
                if nrw in self.chrome['desk']:
                    self.chrome['osys'][key].desktops.add(self.chrome['desk'][nrw])

            self.chrome['osys'][key].save()

    #####################################################################################

    def core_lang(self,model,title, **options):
        self.lang = {}

        for key,lbl in tqdm(dict(
            en='english',fr='french',de='german',
            sp='spanish',ar='arabic',ru='russian',
            zh='chinese',
        ).iteritems(), desc=title):
            self.lang[key],rsp = model.objects.get_or_create(code=key,name=lbl)

            self.lang[key].flag = key

            self.lang[key].save()

            self.lang[lbl] = self.lang[key]

    #####################################################################################

    def auth_users(self,model,title, **options):
        self.iden = {}

        for key,cfg in tqdm(self.conf.iteritems(), desc=title):
            self.iden[key],rsp = model.objects.get_or_create(username=cfg['alias'],email=cfg['email'])

            self.iden[key].first_name = cfg['admin'].get('first',"")
            self.iden[key].last_name = cfg['admin'].get('last',"")

            self.iden[key].is_superuser = cfg['admin'].get('state',False)
            self.iden[key].set_password(cfg['token'].get('passwd','Testing007'))
            self.iden[key].is_staff = cfg['admin'].get('staff',False)

            self.iden[key].save()

            if self.iden[key].username=='tayamino':
                self.user = self.iden[key]

    #************************************************************************************

    def auth_organ(self,model,title, **options):
        self.orgs = {}

        for cfg in tqdm([
            dict(owner='fayssal', alias='azalinc',email='azal.inc@gmail.com'),
            dict(owner='tayamino',alias='itissal',email='it.issal@gmail.com'),
        ], desc=title):
            self.orgs[cfg['alias']],rsp = model.objects.get_or_create(
                owner=self.iden[cfg['owner']],
                alias=cfg['alias'],
            )

            #self.orgs[key].set_password('Testing007')

            self.orgs[cfg['alias']].save()

    #************************************************************************************

    def auth_agent(self,model,title, **options):
        for key,cfg in tqdm(self.conf.iteritems(), desc=title):
            if 'robot' in cfg:
                obj,rsp = model.objects.get_or_create(
                    owner=self.orgs['itissal'],
                    alias=cfg['robot'],
                )

                obj.admin = self.user

                obj.master_key = uuid4().hex
                obj.rest_token = uuid4().hex

                obj.save()

                for sub in ('token','cloud'):
                    for nrw,pki in cfg.get(sub,{}).iteritems():
                        if nrw not in ('passwd','heroku'):
                            sec,rst = BotToken.objects.get_or_create(
                                owner=obj,
                                #alias='default',
                                place=nrw,
                            )

                            sec.value = pki

                            sec.save()

    #####################################################################################

    def sys_host(self,model,title, **options):
        self.host = {}

        for cfg in [
            dict(owner='itissal', alias='tayamino',
                target='ftp.cluster026.hosting.ovh.net',
            pseudo="tayaameito", passwd="tayaameito"),
            dict(owner='azalinc', alias='fayssal',
                target='ftp.cluster020.hosting.ovh.net',
            pseudo="azalinccdm", passwd="azalinccdm"),
        ]:
            self.host[cfg['alias']],rsp = model.objects.get_or_create(
                owner=self.orgs[cfg['owner']],
                alias=cfg['alias'],
            )

            self.host[cfg['alias']].target = cfg['target']
            self.host[cfg['alias']].pseudo = cfg['pseudo']
            self.host[cfg['alias']].passwd = cfg['passwd']

            self.host[cfg['alias']].save()

    #************************************************************************************

    def sys_site(self,model,title, **options):
        for key in tqdm([
            'food','gear','laws','prog','root',
            ##'meta','data','know',#'sens',
            #'laws','work','sale',
            #'book','food','play',
            #'cars','home','immo','desk',
            #'gear','jobs','root','apps',
        ], desc=title):
            obj,rsp = model.objects.get_or_create(
                owner=self.orgs['itissal'],
                alias=key,
                #frame='wp',
                where=self.host['tayamino'],
            )

            obj.target = "https://%s.tayaa.me" % key
            obj.pseudo = self.user.email
            obj.passwd = "tayamino"

            obj.save()

    #####################################################################################

    def pay_shopify(self,model,title, **options):
        for cfg in tqdm(self.spec['shopify'].get('catalog',[]), desc=title):
                #if cfg['owner'] in self.iden:
                obj,rsp = model.objects.get_or_create(
                    user=self.user,
                    #user=self.iden[cfg['user']],
                    fqdn=cfg['fqdn'],
                )

                obj.name = cfg['name']

                obj.save()

    #####################################################################################

    def hrk_owner(self,model,title, **options):
        for key,cfg in tqdm(self.conf.iteritems(), desc=title):
            if cfg['token'].get('heroku',''):
                obj,rsp = model.objects.get_or_create(
                    owner=self.orgs['itissal'],
                    alias=cfg['alias'],
                )

                obj.email = cfg.get('email','')
                obj.token = cfg['token'].get('heroku','')

                obj.save()

        for cfg in tqdm(self.spec['heroku'].get('account',[]), desc=title):
                #if cfg['owner'] in self.iden:
                obj,rsp = model.objects.get_or_create(
                    owner=self.orgs[cfg['owner']],
                    alias=cfg['alias'],
                )

                obj.email = cfg.get('email','')
                obj.token = cfg.get('token','')

                obj.save()

    #************************************************************************************

    def hrk_build(self,model,title, **options):
        self.hrkb = {}

        for cfg in self.spec['heroku'].get('buildpack',[]):
            key,url = cfg['name'],cfg.get('link','')

            self.hrkb[key],rsp = model.objects.get_or_create(
                name=key,
            )

            if len(url):
                self.hrkb[key].link = url

            self.hrkb[key].save()

    #************************************************************************************

    def hrk_templ(self,model,title, **options):
        for nrw in os.listdir(settings.rpath('thedisk','specs','catalog')):
            if nrw.endswith('.yaml'):
                data = self.read_yaml('thedisk','specs','catalog',nrw)

                for key,cfg in (data.iteritems()):
                    obj,rsp = model.objects.get_or_create(
                        owner=self.orgs['itissal'],
                        alias=key,
                    )

                    obj.rname = cfg['slug']
                    obj.rlink = "https://github.com/%(user)s/%(repo)s.git" % cfg
                    obj.categ = nrw.replace('.yaml','')

                    for sub in cfg['type']:
                        if sub in self.hrkb:
                            obj.build.add(self.hrkb[sub])

                    obj.gh_user = cfg['user']
                    obj.gh_repo = cfg['repo']

                    obj.save()

    #####################################################################################

    def bkn_cloud(self,model,title, **options):
        trans = dict(
            cache='redis',sqldb='pgsql',
            nosql='mongo',graph='neo4j',
        )

        for key,cfg in tqdm(self.conf.iteritems(), desc=title):
            if 'store' in cfg:
                for nrw,url in (cfg['store'].iteritems()):
                    if (nrw in trans):
                        obj,rsp = model.objects.get_or_create(
                            owner=self.iden[key],
                            alias='default',
                            proto=trans[nrw],
                        )

                        obj.rlink = url

                        if obj.rlink is not None:
                            obj.save()

    #####################################################################################

    def dat_types(self,model,title, **options):
        mapping = [{
            "type": bool, "size": '1', "gql": "Boolean",
            "parse": "Boolean", "rdf": "",
        },{
            "type": int, "size": '1', "gql": "Integer",
            "parse": "Number", "rdf": "",
        },{
            "type": long, "size": '1', "gql": "Long",
            "parse": "Number", "rdf": "",
        },{
            "type": float, "size": '1', "gql": "Float",
            "parse": "Number", "rdf": "",
        },{
            "type": str, "size": '1', "gql": "String",
            "parse": "String", "rdf": "",
        },{
            "type": unicode, "size": '1', "gql": "String",
            "parse": "String", "rdf": "",
        }]

        mapping += [{
            "type": file, "size": '1', "gql": None,
            "parse": "File", "rdf": "",
        }]

        mapping += [{
            "type": datetime.date, "size": '1', "gql": None,
            "parse": "DateTime", "rdf": "",
        },{
            "type": datetime.time, "size": '1', "gql": None,
            "parse": "DateTime", "rdf": "",
        },{
            "type": datetime.datetime, "size": '1', "gql": None,
            "parse": "DateTime", "rdf": "",
        }]

        mapping += [{
            "type": frozenset, "size": '*', "gql": None,
            "parse": "Array", "rdf": "",
        },{
            "type": set, "size": '*', "gql": None,
            "parse": "Array", "rdf": "",
        },{
            "type": tuple, "size": '*', "gql": None,
            "parse": "Array", "rdf": "",
        },{
            "type": list, "size": '*', "gql": None,
            "parse": "Array", "rdf": "",
        },{
            "type": dict, "size": '*', "gql": None,
            "parse": "JSON", "rdf": "",
        }]

        self.type = {}

        for cfg in mapping:
            std = cfg['type']

            self.type[std],rsp = model.objects.get_or_create(
                name=std.__name__,
            )

            self.type[std].std_py2 = '%s.%s' % (std.__module__, std.__name__)

            for key in ['gql','rdf','parse']:
                if cfg.get(key,None) is not None:
                    setattr(self.type[std], "std_"+key, cfg[key])

            self.type[std].save()

        #std_py2   = models.CharField(max_length=64,blank=True)
        #std_py3   = models.CharField(max_length=64,blank=True)

        #std_csv   = models.CharField(max_length=64,blank=True)
        #std_rdf   = models.CharField(max_length=64,blank=True)

        #std_parse = models.CharField(max_length=64,blank=True)
        #std_wpacf = models.CharField(max_length=64,blank=True)

    def dat_schem(self,model,title, **options):
        obj,rsp = model.objects.get_or_create(
            owner=self.user,
            alias='texte-loi',
        )

        obj.rdf_type = "foaf.Person"
        obj.rdf_link = "http://laws.tayaa.me/wp-linked/texte-loi/%s"

        obj.json_lod = """{
    "name": "http://schema.org/name",
    "link": {"@id": "http://schema.org/url", "@type": "@id"},
    #"image": {"@id": "http://schema.org/image", "@type": "@id"}
}"""

        for item in [
            dict(key='fqdn',std=self.type[str]),
            dict(key='link',std=self.type[str]),
            dict(key='name',std=self.type[str]),
            dict(key='unit',std=self.type[str]),
            #dict(key='text',std=self.type[str]),
        ]:
            f,st = CustomField.objects.get_or_create(
                model=obj,
                alias=item['key'],
            )
            f.vtype = item['std']
            f.save()
            obj.fields.add(f)

        for item in [
            dict(key='docx',std=self.type[str]),
            dict(key='p_ar',std=self.type[str]),
            dict(key='p_fr',std=self.type[str]),
            dict(key='unit',std=self.type[str]),
        ]: # ,'text'):
            f,st = CustomField.objects.get_or_create(
                model=obj,
                alias=item['key'],
            )
            f.vtype = item['std']
            f.save()
            obj.fields.add(f)

        obj.save()

    #####################################################################################

    def nlp_core(self,model,title, **options):
        for lang in tqdm(self.lang.values(), desc=title):
            obj,rsp = model.objects.get_or_create(
                owner=self.user,
                frame='core',
                alias=lang.name,
                speak=lang,
            )

            obj.rname = "stanford-%s-corenlp-2018-10-05-models.jar" % lang.name
            obj.rlink = "https://nlp.stanford.edu/software/" + obj.rname

            obj.save()

    def nlp_tool(self,model,title, **options):
        target = settings.rpath('daten','nltk',"corpora")

        if os.path.exists(target):
            for key in os.listdir(target):
                if key.endswith('.zip'):
                    obj,rsp = model.objects.get_or_create(
                        owner=self.user,
                        frame='nltk',
                        alias=key,
                        speak=self.lang['english'],
                    )

                    obj.rlink = "python://2.7/nltk.corpus.%s" % key

                    obj.save()

    #####################################################################################

    def rich_hdt(self,model,title, **options):
        for key,url in tqdm([
            ('freebase-2013', "freebase-rdf-2013-12-01-00-00.hdt"),
            ('wikidata-2018', "wikidata2018_09_11.hdt"),
            ('dbpedia-2016',  "dbpedia2016-04en.hdt"),

            ('wikidata-2017', "wikidata-20170313-all-BETA.hdt"),
            ('dbpedia-3.9',   "DBPedia-3.9-en.hdt"),
            ('dbpedia-3.8',   "dbpedia-3.8-en.hdt"),

            #('xxxxxx',"yyyyyy")
            ('dblp',          "dblp-20170124.hdt"),
            ('yago2s',        "yago2s-2013-05-08.hdt"),
            ('linkedgeodata', "linkedgeodata-2012-09-10.hdt"),
            ('geonames',      "geonames-11-11-2012.hdt"),
            ('wordnet31',     "wordnet31.hdt"),
            ('swdf',          "swdf-2012-11-28.hdt"),
        ], desc=title):
            obj,rsp = model.objects.get_or_create(
                owner=self.user,
                frame='hdt',
                alias=key,
            )

            obj.rname = url
            obj.rlink = "http://downloads.linkeddatafragments.org/hdt/%s" % obj.rname

            if key not in ('dblp',''):
                obj.rlink = "http://downloads.linkeddatafragments.org/hdt/%s" % obj.rname
            elif key=='wikidata-2018':
                obj.rlink = "http://downloads.linkeddatafragments.org/hdt/wikidata/%s.gz" % obj.rname
            elif key=='dbpedia-2016':
                obj.rlink = "http://fragments.dbpedia.org/hdt/" % obj.rname
            else:
                obj.rlink = "http://downloads.linkeddatafragments.org/hdt/%s.gz" % obj.rname

            obj.save()

        for key in ('english','french','german','russian'):
            obj,rsp = model.objects.get_or_create(
                owner=self.user,
                frame='hdt',
                alias='wiktionary',
            )

            obj.rname = "wiktionary_%s_2012-07-21.hdt" % self.lang[key].code
            obj.speak = self.lang[key]
            obj.rlink = "http://rdf-hdt.org/Datasets/" + obj.rname

            obj.save()

    def rich_rdf(self,model,title, **optionsl):
        data = self.read_json('thedisk','graph','rdf-prefix.json')

        if type(data) is dict:
            for name,link in tqdm(data.iteritems(), desc=title):
                obj,rsp = model.objects.get_or_create(
                    owner=self.user,
                    alias=name,
                    frame='rdf',
                )

                obj.rname = name
                obj.shows = True
                obj.rlink = link

                obj.save()

    def rich_cn5(self,model,title, **options):
        for sub,lst in tqdm({
            'https://s3.amazonaws.com/conceptnet/downloads/2018/edges/': [
                ('assertions',     "conceptnet-assertions-5.6.0.csv.gz"),
            ],
            'https://s3.amazonaws.com/conceptnet/downloads/2018/': [
                ('sentences-free', "omcs-sentences-free.txt"),
                ('sentences-more', "omcs-sentences-more.txt"),
            ],
            'https://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/': [
                ('numberbatch-en', "numberbatch-17.06.txt.gz"),
                ('numberbatch-all',"numberbatch-en-17.06.txt.gz"),
            ],
            'http://conceptnet.s3.amazonaws.com/precomputed-data/2016/numberbatch/17.06/': [
                ('numberbatch-h5', "mini.h5"),
            ],
        }.iteritems(), desc=title):
            for key,pth in (lst):
                obj,rsp = model.objects.get_or_create(
                    owner=self.user,
                    frame='cn5',
                    alias=key,
                )

                obj.rname = pth
                obj.rlink = sub + pth

                obj.save()

    #####################################################################################

    def ep_graphql(self,model,title, **options):
        obj,rsp = model.objects.get_or_create(
            proto='gql',
            owner=self.user,
            alias='countries',
        )

        obj.rlink = "http://countries.trevorblades.com/"
        obj.categ = 'production'

        obj.title = "Countries GraphQL endpoint"
        obj.helps = "A public GraphQL API for information about countries, continents, and languages. This project uses Countries List as a data source, so the schema follows the shape of that data, with a few exceptions: The codes used to key the objects in the original data are available as a code"

        obj.shows = True
        obj.query = """{
  countries{
    code
    name
  }
}"""

        obj.save()

        for key in ('apis','demos','proxies'):
            data = self.read_json('thedisk','graph','ql','serve','%s.json' % key)

            for entry in tqdm(data, desc=title):
                obj,rsp = model.objects.get_or_create(
                    proto='gql',
                    owner=self.user,
                    alias=slugify(entry['info']['title']),
                )

                obj.rlink = unicode(entry['url'])
                obj.categ = key

                if entry.get('logo',{}).get('url',""):
                    obj.cover = unicode(entry['logo']['url'])

                obj.title = unicode(entry['info']['title'])
                obj.helps = unicode(entry['info']['description'])

                obj.save()

    def ep_sparql(self,model,title, **options):
        import sparql

        cnx = sparql.Service("https://io.datascience-paris-saclay.fr/sparql", "utf-8", "GET")

        res = cnx.query("""
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT DISTINCT
    ?title ?description ?endpoint
WHERE {
     ?dataset a dcat:Dataset ;
       dct:title ?title ;
       dct:description ?description ;
       dcat:distribution ?distribution .
    ?distribution dcat:mediaType "application/sparql-results+xml" ;
        dcat:accessURL ?endpoint .
}""")

        for name,text,link in tqdm([row for row in res.fetchone()], desc=title):
            obj,rsp = model.objects.get_or_create(
                proto='sparql',
                owner=self.user,
                alias=unicode(name),
            )

            obj.rlink = unicode(link)

            obj.title = unicode(name)
            obj.helps = unicode(text)
            obj.categ = 'io.datascience-paris-saclay.fr'

            try:
                obj.save()
            except:
                pass

        ['https://dbpedia.org/sparql','https://query.wikidata.org/','http://lodlaundromat.org/sparql/']
        # https://www.wikidata.org/wiki/Wikidata:Lists/SPARQL_endpoints
        ['http://sparql.sindice.com/','http://uriburner.com/sparql']
        # https://www.w3.org/wiki/SparqlEndpoints
        ['http://linkedopencommerce.com/sparql','http://lod.openlinksw.com/sparql']
        # https://labs.mondeca.com/sparqlEndpointsStatus/index.html
        ['http://logd.tw.rpi.edu/sparql','http://data.oceandrilling.org/sparql','http://aquarius.tw.rpi.edu:8888/sparql']

    #####################################################################################

    def sight_cv2(self,model,title, **options):
        data = {
            0: 'background',
            1: 'person', 2: 'bicycle', 3: 'car', 4: 'motorcycle', 5: 'airplane', 6: 'bus',
            7: 'train', 8: 'truck', 9: 'boat', 10: 'traffic light', 11: 'fire hydrant',
            13: 'stop sign', 14: 'parking meter', 15: 'bench', 16: 'bird', 17: 'cat',
            18: 'dog', 19: 'horse', 20: 'sheep', 21: 'cow', 22: 'elephant', 23: 'bear',
            24: 'zebra', 25: 'giraffe', 27: 'backpack', 28: 'umbrella', 31: 'handbag',
            32: 'tie', 33: 'suitcase', 34: 'frisbee', 35: 'skis', 36: 'snowboard',
            37: 'sports ball', 38: 'kite', 39: 'baseball bat', 40: 'baseball glove',
            41: 'skateboard', 42: 'surfboard', 43: 'tennis racket', 44: 'bottle',
            46: 'wine glass', 47: 'cup', 48: 'fork', 49: 'knife', 50: 'spoon',
            51: 'bowl', 52: 'banana', 53: 'apple', 54: 'sandwich', 55: 'orange',
            56: 'broccoli', 57: 'carrot', 58: 'hot dog', 59: 'pizza', 60: 'donut',
            61: 'cake', 62: 'chair', 63: 'couch', 64: 'potted plant', 65: 'bed',
            67: 'dining table', 70: 'toilet', 72: 'tv', 73: 'laptop', 74: 'mouse',
            75: 'remote', 76: 'keyboard', 77: 'cell phone', 78: 'microwave', 79: 'oven',
            80: 'toaster', 81: 'sink', 82: 'refrigerator', 84: 'book', 85: 'clock',
            86: 'vase', 87: 'scissors', 88: 'teddy bear', 89: 'hair drier', 90: 'toothbrush'
        }

        for code,name in tqdm(data.iteritems(), desc=title):
            obj,rsp = model.objects.get_or_create(
                alias=name,
                cv_id=code,
            )

            obj.flags = name

            obj.save()

    def sight_tf2(self,model,title, **options):
        data = {
            'default': (
                'frozen_inference_graph.pb',
                'ssd_mobilenet_v1_coco_2017_11_17.pbtxt',
            ),
        }

        for name,link in tqdm(data.iteritems(), desc=title):
            obj,rsp = model.objects.get_or_create(
                owner=self.user,
                frame='tf',
                alias=name,
            )

            obj.model = link[0]
            obj.confs = link[1]

            obj.save()

    def sight_ocr(self,model,title, **options):
        for key in tqdm(['data','fast','best'], desc=title):
            obj,rsp = model.objects.get_or_create(
                owner=self.user,
                frame='ts',
                alias=key,
            )

            if key=='data':
                obj.rname = "tessdata-master.zip"
                obj.rlink = "https://github.com/tesseract-ocr/tessdata/archive/master.zip"
            else:
                obj.rname = "tessdata_%s-master.zip" % key
                obj.rlink = "https://github.com/tesseract-ocr/tessdata_%s/archive/master.zip" % key

            obj.save()

	########################################################################################

    def open_file(self, *target):
        return open(rpath(*target))

    def read_file(self, *target):
        return self.open_file(*target).read()

    def write_file(self, payload, *target):
        with open(rpath(*target),'w+') as f:
            f.write(payload)
        return payload

    def shell(self, program, *arguments):
        stmt = []

        for part in arguments:
            if ' ' in part:
                part = '"%s"' % part

            stmt.append(part)

        return os.system(' '.join([program]+stmt))

	########################################################################################

    def read_yaml(self, *target):
        return yaml.load(self.open_file(*target))

    def write_yaml(self, payload, *target):
        return self.write_file(yaml.dumps(payload), *target)

    def read_json(self, *target):
        return json.load(self.open_file(*target))

    def write_json(self, payload, *target):
        return self.write_file(json.dumps(payload), *target)

