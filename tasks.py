from azalinc.shortcuts import *

from .models import *

####################################################################

@rq_task
def url_download(src,dest):
    tmp = tempfile.mktemp()

    urllib.urlretrieve(src,tmp)

    DISK.save(dest, open(tmp))

####################################################################

@rq_task
def shopping_download(shop_site_id):
    qs = ShopifySite.objects.filter(Q(id=shop_site_id) or Q(pk=shop_site_id))

    if len(qs):
        for key in ('history','product','covers','variant'):
            if not exists(qs[0].path(key)):
                mkdir(qs[0].path(key))

        data = qs[0].datasets.create(when=datetime.now())

        resp = requests.get("http://%s/products.json" % data.site.fqdn).json()

        save_json(resp, data.site.path('original.json'))

        save_json(resp, data.site.path('history','%s.json' % data.name))

        data.save()

        resultat = dict(images=[],variants=[],products=[],covers={})

        for product in resp.get('products', []):
            record = dict(images=[])

            for field in (
                'id', 'handle', 'vendor', 'tags', 'options',
                'title', 'body_html', 'product_type', # 'images', 'variants',
                'created_at', 'updated_at', 'published_at',
            ):
                record[field] = product[field]

            record['summary'] = record.get('body_html','')

            if 240<len(record['summary']):
                record['summary'] = record['summary'][:236] + ' ...'

            for picture in product.get('images', []):
                for key in picture.get('variant_ids',[]):
                    resultat['covers'][key] = picture

                entry = {}

                for field in (
                    'id', 'product_id', 'created_at', 'updated_at',
                    'src', 'width', 'height', 'position',
                ):
                    entry[field] = picture[field]

                entry['path'] = qs[0].path('covers','%s.jpg' % entry['id'])

                resultat['images'].append(entry)

                record['images'].append(entry)

            for variant in product.get('variants', []):
                entry = {}

                for field in (
                    'id','title','sku','grams','taxable','position','price',
                    'product_id','featured_image', 'compare_at_price',
                    'requires_shipping', 'option1', 'option2', 'option3',
                    'available', 'created_at', 'updated_at', #'published_at',
                ):
                    entry[field] = variant[field]

                resultat['variants'].append(entry)

            resultat['products'].append(record)

        for key in resultat.keys():
            if key not in ['covers']:
                save_json(resultat[key], data.site.path('%s.json' % key))

        if qs[0].down:
            for entry in resultat['images']:
                url_download.delay(entry['src'], entry['path'])

        if not qs[0].keep:
            lst = [x
                for x in os.lsdir(qs[0].path())
                if os.path.isdir(qs[0].path(x))
            ].sorted(reverse=True)

        #qs[0].last = data
        #qs[0].save()
